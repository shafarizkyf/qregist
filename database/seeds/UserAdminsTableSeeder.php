<?php

use Illuminate\Database\Seeder;

class UserAdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/user_admins.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('user_admins')->insert($data);                
    }
}
