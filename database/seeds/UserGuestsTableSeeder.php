<?php

use Illuminate\Database\Seeder;

class UserGuestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/user_guests.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('user_guests')->insert($data);                
    }
}
