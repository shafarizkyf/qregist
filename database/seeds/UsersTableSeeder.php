<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = 'seeder/users.json';
        $data = json_decode(file_get_contents(storage_path($file)), true);
        DB::table('users')->insert($data);
    }
}
