<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizOption extends Model{

    public $timestamps = false;

    public function quiz(){
        return $this->belongsTo('App\Quiz');
    }

}
