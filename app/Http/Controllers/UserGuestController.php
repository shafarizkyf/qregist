<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use App\UserGuest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserGuestController extends Controller{
    
    public function index(){
        if(!Session::has('guest')){
            return redirect(route('home'));
        }

        $guest = UserGuest::whereCode(Session::get('guest'))->first();
        if(!$guest){
            return view('not_found');
        }

        return view('guest.index', compact('guest'));
    }

    public function show($id){
        $guest = UserGuest::whereQrCode($id)->first();
        if(!$guest){
            return view('not_found');
        }

        return view('guest.profile', compact('guest'));
    }

    public function list(){
        $guests = UserGuest::all();
        return view('admin.user_guest', compact('guests'));
    }

    public function store(Request $request){

        $request->validate([
            'name'      => 'required|min:3|max:60',
            'seat'      => 'required',
            'code'      => 'required',
            'potrait'   => 'required|mimes:jpeg,png'
        ]);

        $potrait = $request->file('potrait');

        $guest = new UserGuest();
        $guest->seat = $request->seat;
        $guest->name = $request->name;
        $guest->desc = $request->desc;
        $guest->qr_code = $this->randomNumber(100000, 1000000, 1)[0];
        $guest->code = strtoupper($request->code);
        $guest->createdBy()->associate(Auth::user());

        if($potrait){
            $filename = $potrait->getClientOriginalName();
            $extension = $potrait->getClientOriginalExtension();
            $rename = md5($filename) . time() . '.' . $extension;
            $potrait->move('uploads/guest', $rename);
            $guest->potrait = $rename;
        }

        $guest->save();

        $user = new User();
        $user->username = $request->code;
        $result = $guest->user()->save($user);

        return redirect()->back()->with('success', 'Behasil menambah tamu');
    }

    public function qrCode(){
        $guestCode = Session::get('guest');
        $guest = UserGuest::whereCode($guestCode)->first();
        
        if(!$guest){
            return view('not_found');
        }

        if($guest->is_verified){
            return redirect(route('user.guest.index'));
        }

        return view('guest.qr', compact('guest'));
    }

    public function apiVerifyingGuest(Request $request){
        $guest = UserGuest::whereCode($request->code)->first();
        return ['is_verified' => $guest->is_verified];
    }

    public function validateGuest(Request $request){
        if($request->id){
            $guest = UserGuest::whereQrCode($request->id)->first();
            if($guest){

                if($guest->is_verified){
                    return redirect(route('user.guest.index'));
                }

                $guest->verifiedBy()->associate(Auth::user());
                $guest->verified_at = Carbon::now('Asia/Jakarta');
                $guest->save();

                return view('guest.validated');
            }
        }

        return view('not_found');
    }

    public function delete($id){
        UserGuest::find($id)->delete();        
        return redirect()->back()->with('success', 'Berhasil menghapus data');
    }    

    public function randomNumber($min, $max, $quantity) {
        $numbers = range($min, $max);
        shuffle($numbers);
        return array_slice($numbers, 0, $quantity);
    }

    public function seat(){
        $guest = UserGuest::whereCode(Session::get('guest'))->first();
        return view('guest.seat', compact('guest'));
    }

    public function rundown(){
        return view('guest.rundown');
    }

    public function module(){
        return 'should be file to download';
    }

    public function photo(){
        return view('guest.photo');
    }

}
