<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use App\User;
use App\UserAdmin;
use Illuminate\Http\Request;

class UserAdminController extends Controller{

    public function index(){
        return view('admin.index');
    }

    public function indexOfficer(){
        return view('admin.index_officer');
    }

    public function list(){
        $users = UserAdmin::active()->get();
        return view('admin.user_admin', compact('users'));
    }

    public function store(Request $request){

        $request->validate([
            'name'          => 'required|min:3|max:60',
            'username'      => 'required|min:3|max:30',
            'role'          => 'required',
            'password'      => 'required|min:6|max:60',
            'repassword'    => 'required|same:repassword',
            'potrait'       => 'required|mimes:jpeg,png'
        ]);

        $potrait = $request->file('potrait');

        $admin = new UserAdmin();
        $admin->name = $request->name;
        $admin->role = $request->role;

        if($potrait){
            $filename = $potrait->getClientOriginalName();
            $extension = $potrait->getClientOriginalExtension();
            $rename = md5($filename) . time() . '.' . $extension;
            $potrait->move('uploads/admin', $rename);
            $admin->potrait = $rename;
        }

        $admin->save();

        $user = new User();
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $result = $admin->user()->save($user);

        return redirect()->back()->with('success', 'Berhasil menambah petugas');
    }

    public function delete($id){
        $admin = UserAdmin::find($id);
        $admin->is_active = false;
        $admin->save();

        return redirect()->back()->with('success', 'Berhasil menghapus petugas');
    }

}
