<?php

namespace App\Http\Controllers;

use DB;
use App\Quiz;
use App\QuizOption;
use Illuminate\Http\Request;

class QuizController extends Controller{

    public function index(){
        return view('guest.quiz');
    }

    public function end(){
        return view('guest.quiz_end');
    }

    public function store(Request $request){

        $response = false;
        DB::transaction(function() use ($request, &$response){

            $quiz = new Quiz();
            $quiz->question = $request->question;
            $quiz->save();

            foreach($request->options as $option){
                $options = new QuizOption();
                $options->option = $option;
                $options->quiz()->associate($quiz);
                $options->save();
            }

            $response = true;
        });

        return [$response];
    }

}
