<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Artisan;
use Session;
use App\User;
use App\UserGuest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller{

    public function fix(){
        Artisan::call('config:cache');
        Artisan::call('view:clear');
        Artisan::call('optimize');
        return 'fixed';
    }

    public function login(Request $request){
        
        if(Auth::check()){
            return $this->redirect(Auth::user());
        }
        
        if($request->isMethod('GET')){
            return view('guest.login');
        }
        
        if($request->as == 'officer'){
            $user = User::whereUsername($request->username)->first();
            if($user && !$user->userable->is_active){
                return view('not_found');
            }

            $credential = $request->only('username', 'password');
            if($auth = Auth::attempt($credential)){
                return $this->redirect(Auth::user());
            }
        }

        $guest = UserGuest::whereCode(strtoupper($request->code))->first();
        if($guest){
            Session::put('guest', strtoupper($request->code));
            return redirect(route('user.guest.qr'));
        }

        return view('not_found');
    }

    public function guest(){
        return view('guest.login');
    }

    public function admin(){
        if(Auth::check()){
            return $this->redirect(Auth::user());
        }
        
        return view('admin.login');
    }

    public function logout(){
        Auth::logout();
        Session::flush();
        return redirect(route('home'));
    }

    public function redirect($auth){
        if($auth->userable_type == 'App\\UserAdmin'){
            if($auth->userable->role == 'admin'){
                $route = 'user.admin.index';
            }elseif($auth->userable->role == 'officer'){
                $route = 'user.officer.index';
            }
        }else{
            $route = 'user.guest.qr';
        }

        return redirect(route($route)); 
    }

}
