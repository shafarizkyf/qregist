<?php

namespace App\Http\Controllers;

use DB;
use App\Game;
use App\Quiz;
use App\GameSession;
use Illuminate\Http\Request;

class GameSessionController extends Controller {

    public function store(Request $request){
        $session = new GameSession();
        $session->name = $request->name;
        $session->desc = $request->desc;
        $session->time = $request->time;
        $session->save();
    }

    public function group(Request $request){

        foreach($request->groups as $group){
            $gameSession = GameSession::find($group['game_session_id']);
            $quiz = Quiz::find($group['quiz_id']);

            $game = new Game();
            $game->gameSession()->associate($gameSession);
            $game->quiz()->associate($quiz);
            $game->save();
        }

        return 'okee';
    }
}
