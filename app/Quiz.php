<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model{

    public $timestamps = false;

    public function answer(){
        return $this->belongsTo('App\QuizOption');
    }

}
