<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserGuest extends Model{
    
    public function getIsVerifiedAttribute(){
        return $this->verified_at && $this->verified_by;
    }

    public function getPotraitAttribute($value){
        return $value ? asset('uploads/guest/' . $value) : 'https://api.adorable.io/avatars/150/abott@adorable.png';
    }

    public function getVerifiedAtAttribute($value){
        return $value ? Carbon::parse($value) : null;
    }

    public function getCreatedAtAttribute($value){
        return $value ? Carbon::parse($value) : null;
    }
    
    public function user(){
        return $this->morphOne('App\User', 'userable');
    }

    public function createdBy(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function verifiedBy(){
        return $this->belongsTo('App\User', 'verified_by');
    }

    public function scopeActive($query, $isActive=true){
        return $query->whereIsActive($isActive);
    }

}
