<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model{
    
    public $timestamps = false;

    public function gameSession(){
        return $this->belongsTo('App\GameSession');
    }

    public function quiz(){
        return $this->belongsTo('App\Quiz');
    }

}
