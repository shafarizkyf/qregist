<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAdmin extends Model{

    public $timestamps = false;

    public function getJobAttribute(){
        return $this->role == 'admin' ? 'Admin' : 'SPG';
    }

    public function getPotraitAttribute($value){
        return $value ? asset('uploads/admin/' . $value) : 'https://api.adorable.io/avatars/150/abott@adorable.png';
    }

    public function user(){
        return $this->morphOne('App\User', 'userable');   
    }

    public function scopeActive($query, $isActive=true){
        return $query->whereIsActive($isActive);
    }

}
