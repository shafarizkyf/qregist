<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('fix', 'Auth\LoginController@fix');

Route::get('/', 'Auth\LoginController@guest')->name('home');
Route::get('login', 'Auth\LoginController@admin')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('guest/qr', 'UserGuestController@qrCode')->name('user.guest.qr');
Route::get('user/guest', 'UserGuestController@index')->name('user.guest.index');

Route::get('seat', 'UserGuestController@seat')->name('seat');
Route::get('rundown', 'UserGuestController@rundown')->name('rundown');
Route::get('module', 'UserGuestController@module')->name('module');
Route::get('photo', 'UserGuestController@photo')->name('photo');
Route::get('quiz', 'QuizController@index')->name('quiz.index');
Route::get('quiz-end', 'QuizController@end')->name('quiz.end');

Route::middleware('auth')->group(function(){
    Route::get('user/admin', 'UserAdminController@index')->name('user.admin.index');
    Route::get('user/officer', 'UserAdminController@indexOfficer')->name('user.officer.index');
    
    Route::post('user/admin/create', 'UserAdminController@store')->name('user.admin.create');
    Route::post('user/guest/create', 'UserGuestController@store')->name('user.guest.create');
    
    Route::get('user/admin/list', 'UserAdminController@list')->name('user.admin.list');
    Route::get('user/guest/list', 'UserGuestController@list')->name('user.guest.list');
    Route::get('user/guest/{id}', 'UserGuestController@show')->name('user.guest.profile');
    Route::get('validate-guest', 'UserGuestController@validateGuest')->name('user.guest.validate');

    Route::post('user/admin/delete/{id}', 'UserAdminController@delete')->name('user.admin.delete');
    Route::post('user/guest/delete/{id}', 'UserGuestController@delete')->name('user.guest.delete');
    
    Route::post('quiz/create', 'QuizController@store')->name('quiz.create');
    Route::post('quiz/group', 'GameSessionController@group')->name('quiz.group');
});


