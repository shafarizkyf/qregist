@extends('layouts.app')

@section('content')
  <div class=" full flexCenter">
    <div class="box-login" style="overflow: hidden;">
      <div style="text-align: center;width: 100%">
        <div class="mb30" style="text-align: center;display: inline-block;">
          <div class="box-img-profile">
            <div class="box-img-profile-inner">
              <img class="img-login" src="{{ asset('assets/image/logo.png') }}">
            </div>
          </div>
        </div>
        <form action="{{ route('login') }}" method="POST">
          {{ csrf_field() }}
          <input type="hidden" name="as" value="officer">
          <div class="mb15">
            <input class="inputLogin" type="text" name="username" placeholder="Username" required>
          </div>
          <div class="mb15">
            <input class="inputLogin" type="password" name="password" placeholder="Password" required>
          </div>
          <div class="mb15">
            <button class="btn btn-login">Login</button>
          </div>        
        </form>
        <div class="mb15">
          <a class="aSmallGrey white" href="{{ route('home') }}" style="">Masuk sebagai tamu</a>
        </div>
      </div>
    </div>
  </div>
@endsection