@extends('layouts.app')

@section('content')
  <div class="full flexCenter">
    <div class="box-login" style="overflow: hidden;">
      <div class="w100" >
        <div class="w100 mb20" >
          <div class="centered">
            <span class="img-verifikasi-tamu">
              <img src="{{ asset('assets/image/woman.png') }}">
            </span>
          </div>
        </div>
        <div class="w100 mb15" >
          <div class="centered">
            <div class="">
              <p class="fw800 f20"><b>Siap Menjalankan Tugas!</b></p>
            </div>
          </div>
        </div>
        <div class="w100 mb15" >
          <div class="centered">
            <div class="">
              <p class="f12">Anda sudah dapat memverifikasi tamu dengan <br>membaca QR code tamu Anda</p>
            </div>
          </div>
        </div>
        <div class="w100 mb15 mt50" >
          <div class="centered">
            <a href="{{ route('logout') }}" class="btn btn-peach" style="width: 250px; color:white;">LOGOUT</a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection