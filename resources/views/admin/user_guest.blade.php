@extends('layouts.admin')

@section('content')
  <div class="container-fluid">
    <div class="text-right mB-10">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        <i class="ti-plus"></i> Tambah Tamu
      </button>
    </div>        
    <div class="row">
      <div class="col-md-12">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
          <h4 class="c-grey-900 mB-20">List Tamu</h4>
          <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th></th>
                <th>Nama</th>
                <th>Seat</th>
                <th>Kode</th>
                <th>Tedaftar oleh</th>
                <th>Waktu</th>
                <th>Diverifikasi Oleh</th>
                <th>Waktu</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach($guests as $guest)
              <tr>
                <td width="10%"><img src="{{ $guest->potrait }}" alt="user potrait" width="100"></td>
                <td>{{ $guest->name }}</td>
                <td>{{ $guest->seat }}</td>
                <td>{{ $guest->code }}</td>
                <td>
                  @if($guest->created_by)
                    {{ $guest->createdBy->username }}
                  @else
                    <span class="badge badge-warning">Menunggu Verifikasi</span>
                  @endif
                </td>
                <td>
                  @if($guest->created_at)
                    {{ $guest->created_at->format('d-m-Y H:i') }}
                  @else
                    <span class="badge badge-warning">Menunggu Verifikasi</span>
                  @endif
                </td>
                <td>
                  @if($guest->verified_by)
                    {{ $guest->verifiedBy->username }}
                  @else
                    <span class="badge badge-warning">Menunggu Verifikasi</span>
                  @endif
                </td>
                <td>
                  @if($guest->verified_at)
                    {{ $guest->verified_at->format('d-m-Y H:i') }}
                  @else
                    <span class="badge badge-warning">Menunggu Verifikasi</span>
                  @endif
                </td>
                <td>
                  <form action="{{ route('user.guest.delete', $guest->id) }}" method="POST">
                    {{ csrf_field() }}
                    <button class="btn btn-xs btn-danger" onclick="return confirm('Anda akan menghapus data ini ?')"><i class="ti-trash"></i> Hapus</button>
                  </form>    
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Tamu</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <form action="{{ route('user.guest.create') }}" id="addForm" method="POST" enctype="multipart/form-data">
          <div class="modal-body">
            {{ csrf_field() }}

            <div class="form-group">
              <label for="name">Nama</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="seat">Seat</label>
                  <input type="text" class="form-control" id="seat" name="seat" placeholder="Seat" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="code">Code</label>
                  <input type="text" class="form-control" id="code" name="code" placeholder="Code" required>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="potrait">Foto</label>
              <input type="file" class="form-control" id="potrait" name="potrait" accept="image/x-png,image/gif,image/jpeg" required>
            </div>
            
            <div class="form-group">
              <label for="desc">Keterangan</label>
              <textarea name="desc" id="desc" cols="10" rows="5" class="form-control" placeholder="Keterangan"></textarea>
            </div>
  
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>    
@endsection


@section('js-bottom')
  <script>
    $(function(){
      $("#addForm").validate({
        rules: {
          name: {
            required: true,
            minlength: 3,
            maxlength: 60,
          },
          seat: {
            required: true,
          },
          code: {
            required: true
          },
          potrait:{
            required: true
          }
        }
      });
    });
  </script>
@endsection