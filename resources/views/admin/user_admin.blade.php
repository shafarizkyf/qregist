@extends('layouts.admin')

@section('content')
  <div class="container-fluid">
    <div class="text-right mB-10">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        <i class="ti-plus"></i> Tambah Petugas
      </button>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="bgc-white bd bdrs-3 p-20 mB-20">
          <h4 class="c-grey-900 mB-20">List Petugas</h4>
          <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th></th>
                <th>Username</th>
                <th>Nama</th>
                <td>Tugas</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              @foreach($users as $user)
              <tr>
                <td width="15%"><img src="{{ $user->potrait }}" alt="user potrait" width="100"></td>
                <td>{{ $user->user->username }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->job }}</td>
                <td>
                  <form action="{{ route('user.admin.delete', $user->id) }}" method="POST">
                    {{ csrf_field() }}
                    <button class="btn btn-xs btn-danger" onclick="return confirm('Anda akan menghapus data ini ?')"><i class="ti-trash"></i> Hapus</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div> 
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Petugas</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <form action="{{ route('user.admin.create') }}" id="addForm" method="POST" enctype="multipart/form-data">
          <div class="modal-body">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="username">Username</label>
              <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="repassword">Konfirmasi Password</label>
                  <input type="password" class="form-control" id="repassword" name="repassword" placeholder="Konfirmasi Password" required>
                </div>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="role">Tugas</label>
                  <select name="role" id="role" class="form-control" required>
                    <option value="">Pilih Tugas</option>
                    <option value="admin">Admin</option>
                    <option value="officer">SPG</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="potrait">Foto</label>
                  <input type="file" class="form-control" id="potrait" name="potrait" required accept="image/x-png,image/gif,image/jpeg">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('js-bottom')
  <script>
    $(function(){
      $("#addForm").validate({
        rules: {
          username:{
            required: true,
            minlength: 3,
            maxlength: 30
          },
          password:{
            required: true,
            minlength: 6,
            maxlength: 60
          },
          repassword:{
            equalTo: '#password',
          },
          name:{
            required: true,
            minlength: 3,
            maxlength: 60,
          },
          role: 'required',
          potrait: {
            required: true,
          }
        }
      });
    });
  </script>
@endsection