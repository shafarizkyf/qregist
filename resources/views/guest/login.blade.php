@extends('layouts.app')

@section('content')
<div class=" full flexCenter">
  <div class="box-login" style="overflow: hidden;">
    <div style="text-align: center;width: 100%">
      <div class="mb30" style="text-align: center;display: inline-block;">
        <div class="box-img-profile">
          <div class="box-img-profile-inner">
            <img class="img-login" src="{{ asset('assets/image/logo.png') }}">
          </div>
        </div>
      </div>
      <form action="{{ route('login') }}" method="POST">
        {{ csrf_field() }}
        <div class="mb15">
          <input class="inputLogin" type="text" name="code" placeholder="Code" required>
        </div>
        <div class="mb15">
          <button class="btn btn-login ">Masuk</button>
        </div>      
      </form>
      <div class="mb15">
        <a class="aSmallGrey white" href="{{ route('login') }}" style="">Masuk sebagai petugas</a>
      </div>
    </div>
  </div>
</div>
@endsection