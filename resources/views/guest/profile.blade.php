@extends('layouts.app')

@section('content')
  <div class="col-xs-12 centered">
    <div class="w300 display-inline-block mt50 centered">
      <div class="bg-bulat">
          <img class="img-login" src="{{ $guest->potrait }}" style="height: 100%; width: 100%; border-radius: 100px;">
      </div>
    </div>
  </div>
  <div class="col-xs-12 centered">
    <div class="mt20 w300 display-inline-block centered">
      <p style="font-size: 18px"><b>{{ $guest->name }}</b></p>
      <h5>Seat: {{ $guest->seat }}</h5>
    </div>
  </div>
  <div class="col-xs-12 centered">
    <div class="mt30 w300 display-inline-block justify">
      <p style="font-size: 14px; letter-spacing: 0.8px">{{ $guest->desc ? $guest->desc : 'Tidak ada keterangan' }}</p>
    </div>
  </div>
  <div class="col-xs-12 mb50 centered">
    <div class="mt30 w300 display-inline-block justify">
      <a href="{{ route('user.guest.validate', ['id' => $guest->qr_code]) }}" class="btn btn-block btn-peach" style="color: white">VERIFIKASI</a>
    </div>
  </div>
@endsection