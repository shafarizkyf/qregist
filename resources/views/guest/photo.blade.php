@extends('layouts.app')

@section('content')
  <div class="col-xs-12 centered mt50 mb20">
    <div class="mt20 w300 display-inline-block centered">
      <p style="font-size: 22px; font-weight: 900; letter-spacing:2px;"><b>FOTO KEGIATAN</b></p>
    </div>
  </div>
  <div class="col-xs-12 centered">
    <div class="display-inline-block mb20centered">
      <div class="img-mid-content-3">
          <img class="img-login" src="{{ asset('assets/image/seminar1.jpg') }}">
      </div>
    </div>
  </div>
  <div class="col-xs-12 centered">
    <div class="display-inline-block mb20 centered">
      <div class="img-mid-content-3">
          <img class="img-login" src="{{ asset('assets/image/seminar2.jpg') }}">
      </div>
    </div>
  </div>
  <div class="col-xs-12 centered">
    <div class="display-inline-block mb20 centered">
      <div class="img-mid-content-3" style="max-height: 100%;max-width: 100%">
          <img class="img-login" src="{{ asset('assets/image/seminar3.jpg') }}" style="">
      </div>
    </div>
  </div>
  <div class="col-xs-12 mb50 centered">
    <div class="mt30 w300 display-inline-block justify">
      <a href="{{ route('user.guest.index') }}" style="text-decoration: none;color: white">
        <button class="btn btn-block btn-peach">MAIN MENU</button>
      </a>
    </div>
  </div>
@endsection