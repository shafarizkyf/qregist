@extends('layouts.app')

@section('content')
  <div class="col-xs-12 centered">
    <div class="display-inline-block mb20 mt50 centered">
      <div class="img-mid-content">
          <img class="img-login" src="{{ asset('assets/image/biru.png') }}" style="width: 100%;height: auto;">
      </div>
    </div>
  </div>
  <div class="col-xs-12 centered mb15">
    <div class="mt20 w300 display-inline-block centered">
      <p style="font-size: 16px; font-weight: 900; letter-spacing:2px;"><b>Hore Anda Berhasil menyelesaikan permainan ini!</b></p>
    </div>
  </div>
  <div class="col-xs-12 centered mb30">
    <div class="mt20 w300 display-inline-block centered">
      <p style="font-size: 12px;">Terimakasih telah berpartisipasi dalama keseruan permainan ini</p>
    </div>
  </div>
  <div class="col-xs-12 mb75 mt50 centered">
    <div class="w300 display-inline-block">
      <a href="{{ route('user.guest.index') }}" style="text-decoration: none;color: white">
        <button class="btn btn-block btn-peach plr20"> &ensp;
          MAIN MENU
        </button>
      </a>
    </div>
  </div>
@endsection