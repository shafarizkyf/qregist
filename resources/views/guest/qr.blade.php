@extends('layouts.app')

@section('content')
  <div class="full flexCenter">
    <div class="box-login" style="overflow: hidden;">
      <div class="w100" >
        <div class="w100 mb15" >
          <div class="centered">
            <div class="">
              <p><b>VERIFIKASI TAMU</b></p>
            </div>
          </div>
        </div>
        <div class="w100 mb30 xs-mb20" >
          <div class="centered">
            <div class="field-qr">
              <span id="guest-code" data-guest-code="{{ $guest->code }}"></span>
              {!! QrCode::size(200)->generate(route('user.guest.profile', $guest->qr_code)) !!}
            </div>
          </div>
        </div>
        <div class="w100" >
          <div class="centered">
            <span class="img-verifikasi-tamu">
              <img src="{{ asset('assets/image/standing-22.png') }}">
          </span>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js-bottom')
<script>
  $(function(){
    let code = $('#guest-code').data('guest-code');
    
    function checking(){
      $.getJSON('http://qregist.shafarizkyf.com/api/status-guest?code=' + code).done(response => {
        if(response.is_verified){
          location.reload();
        }
      });
    }

    setInterval(checking, 3000);
  });
</script>
@endsection


