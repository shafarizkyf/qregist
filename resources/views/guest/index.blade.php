@extends('layouts.app')

@section('content')
  <div class="col-xs-12 centered">
    <div class="w300 display-inline-block centered">
      <div class="bg-bulat">
          <img class="img-login" src="{{ $guest->potrait }}" style="height: 100%; width: 100%; border-radius: 100px;">
      </div>
    </div>
  </div>
  <div class="col-xs-12 centered">
    <div class="w300 display-inline-block centered">
      <p style="font-size: 18px"><b>{{ $guest->nama }}</b></p>
    </div>
  </div>
  <div class="col-xs-12 mb15 centered">
    <div class="w300 display-inline-block">
      <a href="{{ route('seat') }}" class="btn btn-block btn-peach" style="text-align: left; color:white;"> &ensp;
        <img class="img-login" src="{{ asset('assets/image/director-chair.svg') }}" style="height: 30px;"> &ensp;
        Nomor Bangku : {{ $guest->seat }}
      </a>
    </div>
  </div>
  <div class="col-xs-12 mb15 centered">
    <div class="w300 display-inline-block">
      <a href="{{ route('rundown') }}" class="btn btn-block btn-peach" style="text-align: left; color:white;"> &ensp;
        <img class="img-login" src="{{ asset('assets/image/poster.svg') }}" style="height: 30px;"> &ensp;
        Rundown Kegiatan
      </a>
    </div>
  </div>
  <div class="col-xs-12 mb15 centered">
    <div class="w300 display-inline-block">
      <a href="{{ route('module') }}" class="btn btn-block btn-peach" style="text-align: left; color:white;"> &ensp;
        <img class="img-login" src="{{ asset('assets/image/open-book') }}.svg" style="height: 30px;"> &ensp;
        Materi Kegiatan
      </a>
    </div>
  </div>
  <div class="col-xs-12 mb15 centered">
    <div class="w300 display-inline-block">
      <a href="{{ route('photo') }}" class="btn btn-block btn-peach" style="text-align: left; color:white;"> &ensp;
        <img class="img-login" src="{{ asset('assets/image/picture.svg') }}" style="height: 30px;"> &ensp;
        Foto Kegiatan
      </a>
    </div>
  </div>
  <div class="col-xs-12 mb15 centered">
    <div class="w300 display-inline-block">
      <a href="{{ route('quiz.index') }}" class="btn btn-block btn-peach" style="text-align: left; color:white;"> &ensp;
        <img class="img-login" src="{{ asset('assets/image/options.svg') }}" style="height: 30px;"> &ensp;
        Games
      </a>
    </div>
  </div>
  <div class="col-xs-12 mb15 centered">
    <div class="w300 display-inline-block">
      <a href="{{ route('logout') }}" class="btn btn-block btn-peach" style="text-align: left; color:white"> &ensp;
        <img class="img-login" src="{{ asset('assets/image/logout.svg') }}" style="height: 30px;"> &ensp;
        Logout
      </a>
    </div>
  </div>
@endsection