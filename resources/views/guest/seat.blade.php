@extends('layouts.app')

@section('content')
  <div class="col-xs-12 centered">
    <div class="display-inline-block mt50 centered">
      <div class="img-mid-content-bangku">
          <img class="img-login" src="{{ asset('assets/image/bangku.jpg') }}" style="width: 100%;height: auto;">
      </div>
    </div>
  </div>
  <div class="col-xs-12 centered">
    <div class="mt30 w300 display-inline-block centered">
      <p class="f20 ls15 fw800"><b>ANDA BERADA DI {{ $guest->seat }}</b></p>
    </div>
  </div>
  <div class="col-xs-12 mb50 centered">
    <div class="mt30 w300 display-inline-block justify">
      <a href="{{ route('user.guest.index') }}" style="text-decoration: none;color: white">
        <button class="btn btn-block btn-peach">MAIN MENU</button>
      </a>
    </div>
  </div>
@endsection