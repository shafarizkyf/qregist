@extends('layouts.app')

@section('content')
  <div class="col-xs-12 centered">
    <div class="display-inline-block mt50 centered">
      <div class="img-mid-content">
          <img class="img-login" src="{{ asset('assets/image/pink.png') }}" style="width: 100%;height: auto;">
      </div>
    </div>
  </div>
  <div class="col-xs-12 centered">
    <div class="mt30 w300 display-inline-block centered">
      <p class="f20 ls15 fw800"><b>VERIFIKASI SUKSES!</b></p>
    </div>
  </div>
  <div class="col-xs-12 centered">
    <div class="mt30 w300 display-inline-block centered">
      <p style="font-size: 12px; letter-spacing: 0.8px">Tamu Anda sekarang dapat mengakses<br>halaman terkait</p>
    </div>
  </div>
  <div class="col-xs-12 mb50 centered">
    <div class="mt30 w300 display-inline-block justify">
      <a href="{{ route('login') }}" class="btn btn-block btn-peach" style="color:white">HOME</a>
    </div>
  </div>
@endsection