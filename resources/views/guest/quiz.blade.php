@extends('layouts.app')

@section('content')
  <div class="col-xs-12 centered">
    <div class="display-inline-block mb20 mt50 centered">
      <div class="img-mid-content">
          <img class="img-login" src="{{ asset('assets/image/puzzle2.png') }}" style="width: 100%;height: auto;">
      </div>
    </div>
  </div>
  <div class="col-xs-12 centered mb30">
    <div class="mt20 w300 display-inline-block centered">
      <p style="font-size: 16px; font-weight: 900; letter-spacing:2px;"><b>Ini pertanyaan ?</b></p>
    </div>
  </div>
  <div class="col-xs-12 mb15 centered">
    <div class="w300 display-inline-block">
      <a href="quiz.php" style="text-decoration: none;">
        <a href="{{ route('quiz.end') }}" class="btn btn-block btn-peach plr20" style="color:white;"> &ensp;
          <span>A. Ini jawaban</span>
        </a>
      </a>
    </div>
  </div>
  <div class="col-xs-12 mb15 centered">
    <div class="w300 display-inline-block">
      <a href="quiz.php" style="text-decoration: none;">
        <a href="{{ route('quiz.end') }}" class="btn btn-block btn-peach plr20" style="color:white;"> &ensp;
          <span>B. Ini jawaban lagi</span>
        </a>
      </a>
    </div>
  </div>
  <div class="col-xs-12 mb15 centered">
    <div class="w300 display-inline-block">
      <a href="quiz.php" style="text-decoration: none;">
        <a href="{{ route('quiz.end') }}" class="btn btn-block btn-peach plr20" style="color:white;"> &ensp;
          <span>C. Ini juga jawaban</span>
        </a>
      </a>
    </div>
  </div>
  <div class="col-xs-12 mb15 centered">
    <div class="w300 display-inline-block">
      <a href="quiz.php" style="text-decoration: none;">
        <a href="{{ route('quiz.end') }}" class="btn btn-block btn-peach plr20" style="color:white;"> &ensp;
          <span>D. Jawaban benar yang mana</span>
        </a>
      </a>
    </div>
  </div>
@endsection