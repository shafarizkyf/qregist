<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8" />
	    <title>QRegist</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	    <!-- Bootstap -->
	    <link href="{{ asset('assets/plugins/bootstrap-3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	    <!-- Font awesome -->
	    <link href="{{ asset('assets/plugins/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	    <!-- My grid -->
	    <link href="{{ asset('assets/css/grid_ncn.css') }}" rel="stylesheet" type="text/css" />	    
	    <!-- My size -->
	    <link href="{{ asset('assets/css/size_ncn.css') }}" rel="stylesheet" type="text/css" />
	    <!-- My style -->
			<link href="{{ asset('assets/css/myStyle.css') }}" rel="stylesheet" type="text/css" />
			<style type="text/css">
				@font-face {
						font-family: OptimusPrinceps;
						src: url('{{ public_path('assets/fonts/SEGOEUI.TTF') }}');
				}
			</style>
	</head>
	<style type="text/css">
		body{
			display: flex;
			align-items: center;
			justify-content: center;
			height:100%
		}
	</style>
	<body>
    <section class="containter" style="overflow: hidden;">
      @yield('content')
    </section>
    <script src="{{ asset('assets/plugins/jquery/jquery-3.3.1.min.js') }}"></script>
		<script src="{{ asset('assets/plugins/bootstrap-3/js/bootstrap.min.js') }}"></script>
		@yield('js-bottom')
	</body>
</html>