<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>QRegist</title>
    <style>#loader{transition:all .3s ease-in-out;opacity:1;visibility:visible;position:fixed;height:100vh;width:100%;background:#fff;z-index:90000}#loader.fadeOut{opacity:0;visibility:hidden}.spinner{width:40px;height:40px;position:absolute;top:calc(50% - 20px);left:calc(50% - 20px);background-color:#333;border-radius:100%;-webkit-animation:sk-scaleout 1s infinite ease-in-out;animation:sk-scaleout 1s infinite ease-in-out}@-webkit-keyframes sk-scaleout{0%{-webkit-transform:scale(0)}100%{-webkit-transform:scale(1);opacity:0}}@keyframes sk-scaleout{0%{-webkit-transform:scale(0);transform:scale(0)}100%{-webkit-transform:scale(1);transform:scale(1);opacity:0}}</style>
    <link href="{{ asset('assets/style.css') }}" rel="stylesheet">
    <style>
      .error{
        color: #f44336;
      }
    </style>
  </head>
  <body class="app">
    <div id="loader">
      <div class="spinner"></div>
    </div>
    <script>window.addEventListener('load', () => {
      const loader = document.getElementById('loader');
      setTimeout(() => {
        loader.classList.add('fadeOut');
      }, 300);
      });
    </script>
    <div>
      <div class="sidebar">
        <div class="sidebar-inner">
          <div class="sidebar-logo">
            <div class="peers ai-c fxw-nw">
              <div class="peer peer-greed">
                <a class="sidebar-link td-n" href="{{ route('home') }}">
                  <div class="peers ai-c fxw-nw">
                    <div class="peer">
                      <div class="logo"><img src="{{ asset('assets/static/images/logo.png') }}" alt=""></div>
                    </div>
                    <div class="peer peer-greed">
                      <h5 class="lh-1 mB-0 logo-text">QRegist</h5>
                    </div>
                  </div>
                </a>
              </div>
              <div class="peer">
                <div class="mobile-toggle sidebar-toggle"><a href="" class="td-n"><i class="ti-arrow-circle-left"></i></a></div>
              </div>
            </div>
          </div>
          <ul class="sidebar-menu scrollable pos-r">
            <li class="nav-item dropdown">
              <a class="dropdown-toggle" href="javascript:void(0);"><span class="icon-holder"><i class="c-orange-500 ti-user"></i> </span><span class="title">User</span> <span class="arrow"><i class="ti-angle-right"></i></span></a>
              <ul class="dropdown-menu">
                <li><a class="sidebar-link" href="{{ route('user.admin.list') }}">Petugas</a></li>
                <li><a class="sidebar-link" href="{{ route('user.guest.list') }}">Tamu</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
      <div class="page-container">
        <div class="header navbar">
          <div class="header-container">
            <ul class="nav-left">
              <li><a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a></li>
            </ul>
            <ul class="nav-right">
              <li class="dropdown">
                <a href="" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                  <div class="peer mR-10"><img class="w-2r bdrs-50p" src="{{ Auth::user()->userable->potrait }}" alt=""></div>
                  <div class="peer"><span class="fsz-sm c-grey-900">{{ Auth::user()->userable->name }}</span></div>
                </a>
                <ul class="dropdown-menu fsz-sm">
                  <li><a href="{{ route('logout') }}" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-power-off mR-10"></i> <span>Logout</span></a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        <main class="main-content bgc-grey-100">
          <div id="mainContent">
            <div class="full-container">
              <div class="container-fluid mT-10">
                @if(Session::has('success'))
                  <div class="alert alert-primary" role="alert">{{ Session::get('success') }}</div>                                                
                @elseif(Session::has('warning'))
                  <div class="alert alert-warning" role="alert">{{ Session::get('warning') }}</div>                                
                @elseif(Session::has('danger'))
                  <div class="alert alert-danger" role="alert">{{ Session::get('danger') }}</div>                
                @endif
              </div>
              @yield('content')
            </div>
          </div>
        </main>
      </div>
    </div>
    <script type="text/javascript" src="{{ asset('assets/vendor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bundle.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
    @yield('js-bottom')
  </body>
</html>