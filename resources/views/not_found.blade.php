@extends('layouts.app')

@section('content')
  <div class="full flexCenter">
    <div class="box-login" style="overflow: hidden;">
      <div class="w100" >
        <div class="w100 mb20" >
          <div class="centered">
            <span class="img-verifikasi-tamu-2">
              <img src="{{ asset('assets/image/cat.png') }}">
            </span>
          </div>
        </div>
        <div class="w100 mb15" >
          <div class="centered">
            <div class="">
              <p class="fw800 f20"><b>Ups! Data Tidak Ditemukan</b></p>
            </div>
          </div>
        </div>
        <div class="w100 mb15" >
          <div class="centered">
            <div class="">
              <p class="f12">Sepertinya Anda salah memasukan kode.<br>Silahkan cek kembali atau hubungi kami jika<br>memerlukan bantuan</p>
            </div>
          </div>
        </div>
        <div class="w100 mb15 mt50" >
          <div class="centered">
            <a href="{{ route('login') }}" class="btn btn-peach" style="width: 250px; color:white">Login</a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection